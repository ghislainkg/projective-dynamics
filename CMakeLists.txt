cmake_minimum_required(VERSION 3.5)

SET(CMAKE_CXX_STANDARD 17)
SET(CMAKE_CXX_STANDARD_REQUIRED True)
SET(CMAKE_BUILD_TYPE Debug)

add_definitions(-D_MY_OPENGL_IS_33_)

add_definitions(-DGLM_ENABLE_EXPERIMENTAL)

project(DeformableDynamics)

add_executable(
  ${PROJECT_NAME} 

  src/projective-dynamics/Constraints.cpp
  src/projective-dynamics/Body.cpp
  src/projective-dynamics/PositionBasedDynamics.cpp
  src/projective-dynamics/ProjectiveDynamics.cpp
  src/DynamicEngine.cpp
  src/main.cpp

  # ImGui -------------------------------------------------

  deps/imgui/imgui.cpp
  deps/imgui/imgui_draw.cpp
  deps/imgui/imgui_tables.cpp
  deps/imgui/imgui_widgets.cpp
  deps/imgui/backends/imgui_impl_glfw.cpp
  deps/imgui/backends/imgui_impl_opengl3.cpp
)

target_include_directories(
  ${PROJECT_NAME} PUBLIC
  src
  deps/glad/include)

# OPENMP
find_package(OpenMP)
if (OPENMP_FOUND)
	set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
	set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
	set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

# GLM

add_subdirectory(${CMAKE_SOURCE_DIR}/deps/glm)
target_link_libraries(${PROJECT_NAME} glm)

# EIGEN
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_SOURCE_DIR}/deps/eigen)

# NLOPT
target_link_libraries(${PROJECT_NAME} nlopt)

# GumRender3D -----------------------------------

set(GUMRENDER3D_GLM_SOURCE_DIR ${CMAKE_SOURCE_DIR}/deps/glm CACHE PATH "" FORCE)
set(GUMRENDER3D_GLAD_SOURCE_FILE ${CMAKE_SOURCE_DIR}/deps/glad/src/glad.c CACHE PATH "" FORCE)
set(GUMRENDER3D_GLAD_HEADER_DIR ${CMAKE_SOURCE_DIR}/deps/glad/include CACHE PATH "" FORCE)
set(GUMRENDER3D_ASSIMP_SOURCE_DIR ${CMAKE_SOURCE_DIR}/deps/assimp CACHE PATH "" FORCE)
set(GUMRENDER3D_ASSIMP_HEADER_DIR ${CMAKE_SOURCE_DIR}/deps/assimp/include CACHE PATH "" FORCE)
set(GUMRENDER3D_IMGUI_DIR ${CMAKE_SOURCE_DIR}/deps/imgui CACHE PATH "" FORCE)
set(GUMRENDER3D_IMGUI_BACKENDS_DIR ${CMAKE_SOURCE_DIR}/deps/imgui/backends CACHE PATH "" FORCE)
set(GUMRENDER3D_GLFW_DIR ${CMAKE_SOURCE_DIR}/deps/glfw CACHE PATH "" FORCE)
add_subdirectory(deps/gumrender3d)

target_link_libraries(${PROJECT_NAME} GumRender3D)
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_SOURCE_DIR}/deps/gumrender3d/include)

find_library(HAS_GLFW glfw)
if(HAS_GLFW)
  target_link_libraries(${PROJECT_NAME} glfw)
else()
  add_subdirectory(${CMAKE_SOURCE_DIR}/deps/glfw)
  target_link_libraries(${PROJECT_NAME} glfw)
  target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_SOURCE_DIR}/deps/glfw/include)
endif()
# DL_LIBS
target_link_libraries(${PROJECT_NAME} ${CMAKE_DL_LIBS})
# Imgui
target_include_directories(${PROJECT_NAME} PRIVATE ${GUMRENDER3D_IMGUI_DIR})
target_include_directories(${PROJECT_NAME} PRIVATE ${GUMRENDER3D_IMGUI_BACKENDS_DIR})


# Copy the .DLL (For Windows)
if(WIN32)
	add_custom_command(
		TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND		${CMAKE_COMMAND} -E copy
					${CMAKE_BINARY_DIR}/deps/glfw/src/glfw3d.dll
					${CMAKE_BINARY_DIR}
		COMMAND ${CMAKE_COMMAND} -E echo "dll transfered"
	)
endif()

#!/bin/bash

if [ -d /deps ]; then
  echo "Deps directory exists."
else
  mkdir deps
  echo "Deps directory created."
fi
cd deps

# Assimp
git clone https://github.com/assimp/assimp.git
# Eigen
git clone https://gitlab.com/libeigen/eigen.git
# glfw
git clone https://github.com/glfw/glfw.git
# glm
git clone https://github.com/g-truc/glm.git
# ImGui
git clone -b docking --single-branch https://github.com/ocornut/imgui.git
# gumrender3d
git clone https://gitlab.com/ghislainkg/gumrender3d.git

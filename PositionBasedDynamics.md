# Different approaches

Approaches to deformable bodies simulation.

### Position Based Dynamics
Here the body is represented as a set of points without any connections between them.

### Mass-Spring System
Here the body is represented as a system of point mass attached together by springs.

### Force Based Methods

[Large steps in cloth simulation]

# Position Based Dynamics

## Body representation
In position based dynamics, the body is represented as a set of $N$ vertices. For each vetices, we store its physical properties:
* $x_i$ its position.
* $v_i$ its velocity.
* $m_i$ its mass

## Constrains
We also take into account a set of $M$ constraints that are defined by:
* The $n_i$ indices $\{j_1, ..., j_{n_i}\}$ of the vertices that are affected by the constraint.
* A function $C_i: R^{3n_i} \to R$ That maps a set of $n_i$ points to a real value.
* $k_i \in [0,1] $ a stiffness parameter deffining the strengh of the constraint.
* An equality or inequality.

The $i-th$ constraint is expressed as follow:

$$\begin{align*}
& C_i(x_{j_1}, ..., x_{j_{n_i}}) = 0 \\
& \quad\quad\quad \text{Or } \\
& C_i(x_{j_1}, ..., x_{j_{n_i}}) >= 0
\end{align*}
$$

## Time integration
Given a time step $\Delta t$, we update the properties of each vertices at each time step.
<br>
There are several methods to perform time integration with their advantages and drawbacks:
* **Explicit (Forward) Euler Scheme**:
  * The next position is derived from the **current** position and velocity.
  * The next velocity is derived from the **current** velocity and acceleration.
$$
\begin{align*}
& Y(t+\Delta t) = Y(t) + \Delta t \dot{Y}(t) \\
& \dot{Y}(t+\Delta t) = \dot{Y}(t) + \Delta t \ddot{Y}(t)
\end{align*}
$$

* **Symplectic Euler Scheme**:
  * The next velocity is derived from the **current velocity** and acceleration.
  * The next position is derived from the **current position** and the **next velocity**.
$$
\begin{align*}
& \underline{\dot{Y}(t+\Delta t)} = \dot{Y}(t) + \Delta t \ddot{Y}(t) \\
& Y(t+\Delta t) = Y(t) + \Delta t \underline{\dot{Y}(t+\Delta t)}
\end{align*}
$$

* **Implicit Euler Scheme**:
  * The next velocity is derived from the **current velocity** and **next acceleration**.
  * The next position is derived from the **current position** and the **next velocity**.
$$
\begin{align*}
& \dot{Y}(t+\Delta t) = \dot{Y}(t) + \Delta t \underline{\ddot{Y}(t+\Delta t)} \\
& Y(t+\Delta t) = Y(t) + \Delta t \dot{Y}(t+\Delta t)
\end{align*}
$$

### Update the positions

**First perform time integration.**
We introduce $p_i$ which is the position for the $i-th$ vertex after the integration step. Here we will use the symplectic Euler scheme. But another one can be used as well.
$$
\begin{align*}
& v_i(t+\Delta t) = v_i(t) + \Delta t w_i v_i(t) \\
& p_i(t+\Delta t) = x_i(t) + \Delta t v_i(t+\Delta t)
\end{align*}
$$

**Second determines constraints.**
We determines the constraints to be applied to the $p_i$ to get points that respect them. Constraints on **collision** can be determined by launching a collision detection algorithm for the body with its surrounding and with itself.
<br>
We get a set of $M$ functions $C_i$ hence we will need to solve a system of $M$ equations:

$$
\begin{cases}
& C_i(p_1, \dots, p_N) = 0
& \text{ Or}
& C_i(p_1, \dots, p_N) >= 0 \\
& \text{ with } i \in \{1 \dots M\} \\
\end{cases}
\quad\quad\quad\quad (1)
$$

**Third project $p_i$ on constraints.**
We project the $p_i$ on the space formed by the constraints to get points as close as possible to the original and that respect the constaints. Then we can update the $x_i$ and correct the $v_i$.
<br>
Projecting onto the constraints comes to solving the system $(1)$ of equations. For most of the constraints (like a distance constraint $||p_i-p_j||-d > 0$) are not linear. So **the system is not linear**.

#include "Body.hpp"

void Body::init() {
  xs.clear();
  ps.clear();
  vs.clear();
  Fs.clear();

  // current position start at vertices positions
  xs.reserve(vertices.size());
  for(auto & v : vertices) xs.push_back(v);
  // allocate space for unconstrained positions
  ps.resize(vertices.size(), {0,0,0});
  // initial velocities
  vs.resize(vertices.size(), {0,0,0});
  // External forces: Earth gravity force
  Fs.resize(vertices.size(), {0,-EARTH_G,0});

  extractEdgeTriangles(triangles, edges);

  constraints.clear();
  DistanceConstraint::generateConstraints(
    xs, edges, distanceConstraintStiffness, nIters,
    constraints
  );
  BendingConstraint::generateConstraints(
    xs, edges, bendingConstraintStiffness, nIters,
    constraints
  );
}

void Body::step(float dt, std::vector<Body*> & others) {
  updateConstraintsParams();
  if(method == DynamicMethod::PBD) stepPBD(dt, others);
  else if(method == DynamicMethod::PROJECTIVE_DYNAMICS) stepPD(dt, others);
}

void Body::updateCollisionConstraints(std::vector<Body*> & others) {
  // Remove previous collision constraints
  std::vector<int> rem;
  int i=0;
  for(auto & c : constraints) {
    if(c->getType()==COLLISION) rem.push_back(i);
    i++;
  }
  int offset = 0;
  for(int i : rem) {
    constraints.erase(constraints.begin()+i-offset);
    offset++;
  }

  // Generate new collision constraints
  std::vector<CollisionConstraint::BodyInfo> otherBodies;
  for(auto b : others) {
    otherBodies.push_back({
      &b->xs, &b->triangles, b->isFixed, b->name
    });
  }
  CollisionConstraint::generateConstraints(
    name,
    xs, ps, otherBodies,
    collisionConstraintStiffness, nIters,
    constraints
  );

}

void Body::updateConstraintsParams() {
  for(auto & c : constraints) {
    if(c->getType()==ConstraintType::DISTANCE) {
      c->updateParams(distanceConstraintStiffness, nIters);
    }
    else if(c->getType()==ConstraintType::BENDING) {
      c->updateParams(bendingConstraintStiffness, nIters);
    }
    else if(c->getType()==ConstraintType::COLLISION) {
      c->updateParams(collisionConstraintStiffness, nIters);
    }
    else if(c->getType()==ConstraintType::ATTACH) {
      c->updateParams(1, nIters);
    }
  }
}

#ifndef _PROJECTIVE_DYNAMICS_GEOMETRY_
#define _PROJECTIVE_DYNAMICS_GEOMETRY_

#include "../Utils.hpp"

inline void extractEdges(
  const std::vector<glm::uvec3> & triangles,
  std::vector<glm::uvec2> & edges
) {
  edges.clear();
  for(auto & t : triangles) {
    for(unsigned int i=0; i<3; i++) {
      glm::uvec2 e1 = {t[i],t[(i+1)%3]};
      glm::uvec2 e2 = {t[i],t[(i+1)%3]};
      bool e1Unvisited = std::find(edges.begin(), edges.end(), e1)==edges.end();
      bool e2Unvisited = std::find(edges.begin(), edges.end(), e2)==edges.end();
      if(e1Unvisited && e2Unvisited) {
        edges.push_back(e1);
      }
    }
  }
}

typedef std::pair<glm::uvec2,glm::ivec2> EdgeTriangles;
inline void extractEdgeTriangles(
  const std::vector<glm::uvec3> & ts,
  std::vector<EdgeTriangles> & edges
) {
  edges.clear();
  for(unsigned int t=0; t<ts.size(); t++) {
    auto triangle = ts[t];
    for(int v=0; v<3; v++) {
      // An edge of the triangle
      glm::uvec2 edge = {triangle[v],triangle[(v+1)%3]};
      // The triangle vertex of triangle opposite to the edge
      unsigned int thirdVertex = triangle[(v+2)%3];
      // Is the edge in the list ?
      auto edgeTriangles = std::find_if(
        edges.begin(), edges.end(),
        [&](const EdgeTriangles & e) {
          return 
            (e.first[0]==edge[0]&&e.first[1]==edge[1]) ||
            (e.first[0]==edge[1]&&e.first[1]==edge[0]);
        }
      );
      if(edgeTriangles==edges.end()) {
        // The edge is unknown
        // We add it with its first triangle opposite vertex
        edges.push_back({edge, {thirdVertex,-1}});
      }
      else {
        // The edge is known
        // We add its second triangle opposite vertex
        edgeTriangles->second[1] = thirdVertex;
      }
    }
  }
}

inline bool rayTriangleIntersection(
  const glm::vec3 & A, const glm::vec3 & B, const glm::vec3 & C,
  const glm::vec3 & O, const glm::vec3 & D,
  glm::vec3 & interPoint
) {
  glm::vec3 d = glm::normalize(D);
  glm::vec3 n = glm::normalize(glm::cross(B-A, C-A));
  if(std::abs(glm::dot(n,d))<1e-5) {
    return false;
  }

  // Intersection with the plane
  float t = (glm::dot(n,A) - glm::dot(n,O)) / glm::dot(n,d);
  if(t<0) {
    return false;
  }
  glm::vec3 P = O + t*d;

  if(
    glm::dot(glm::cross(B-A,P-A),n) >= 0 &&
    glm::dot(glm::cross(C-B,P-B),n) >= 0 &&
    glm::dot(glm::cross(A-C,P-C),n) >= 0
  ) {
    interPoint = P;
    return true;
  }
  else {
    return false;
  }
}

inline float getTriangleSolidAngleFromPoint(
  const glm::vec3 & a, const glm::vec3 & b, const glm::vec3 & c,
  const glm::vec3 & point
) {
  glm::vec3 A = a-point;
  glm::vec3 B = b-point;
  glm::vec3 C = c-point;
  float la = glm::length(A);
  float lb = glm::length(B);
  float lc = glm::length(C);
  float ab = glm::dot(a,b);
  float bc = glm::dot(b,c);
  float ca = glm::dot(c,a);
  float tanOfHalf = glm::determinant(glm::mat3(A,B,C)) / 
    (la*lb*lc + lc*ab + la*bc + lb*ca);
  return glm::atan(tanOfHalf*2);
}

inline float getMeshWindingNumber(
  const std::vector<glm::vec3> & vertices,
  const std::vector<glm::uvec3> & triangles,
  const glm::vec3 & point
) {
  float sumSolidAngles = 0;
  for(auto & t : triangles) {
    sumSolidAngles += getTriangleSolidAngleFromPoint(
      vertices[t[0]], vertices[t[1]], vertices[t[2]],
      point
    );
  }
  return (1.f/4*M_PI)*sumSolidAngles;
}

inline float isInSideMesh(
  const std::vector<glm::vec3> & vertices,
  const std::vector<glm::uvec3> & triangles,
  const glm::vec3 & point
) {
  return getMeshWindingNumber(vertices,triangles,point) > M_PI;
}

inline glm::vec3 getCenterOfMass(
  const std::vector<float> & masses,
  const std::vector<glm::vec3> & positions
) {
  glm::vec3 res = {0,0,0};
  float totalMass = 0;
  for(unsigned int i=0; i<masses.size(); i++) {
    res += masses[i]*positions[i];
    totalMass += masses[i];
  }
  res *= 1/totalMass;
  return res;
}

inline float volumeTetraedra(
  const glm::vec3 & pi, const glm::vec3 & pj,
  const glm::vec3 & pk, const glm::vec3 & pl
) {
  return (1/6.f)*std::abs(glm::dot( pi-pk, glm::cross(pj-pk,pl-pk) ));
}

#endif

#include "Body.hpp"

void Body::stepPBD(float dt, std::vector<Body*> & others) {
  if(isFixed) return;

  // Update velocity
  for(unsigned int i=0; i<vs.size(); i++)
    vs[i] = vs[i] + dt*(1/ms[i])*Fs[i];
  // Compute unconstrained particles positions
  for(unsigned int i=0; i<vs.size(); i++)
    ps[i] = xs[i] + dt*vs[i];

  // Generate collision constraints
  updateCollisionConstraints(others);

  auto ws = ms;
  for (auto & w : ws) w = 1/w;
  for(auto & c : constraints) {
    if(c->getType()==ATTACH) {
      AttachConstraint* ac = (AttachConstraint*)c.get();
      ws[ac->i] = 0;
      ps[ac->i] = ac->p;
    }
  }

  // Project constraints
  auto cs = constraints;
  auto rng = std::default_random_engine {};
  std::shuffle(cs.begin(), cs.end(), rng);
  for(unsigned int iter=0; iter<nIters; iter++) {
    for(auto & c : cs) {
      if(c->getType() != ATTACH) {
        c->displace(ps, ms);
      }
    }
  }

  // Correct velocities
  for(unsigned int i=0; i<vs.size(); i++) {
    vs[i] = (1/dt) * (ps[i] - xs[i]);
  }
  // Update particle positions
  for(unsigned int i=0; i<vs.size(); i++) {
    xs[i] = ps[i];
  }
}

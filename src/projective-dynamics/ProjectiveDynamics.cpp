#include "Body.hpp"

#include <Eigen/Dense>
#include <Eigen/Sparse>

#include <nlopt.h>

std::vector<glm::vec3> toStdVector(
	const Eigen::MatrixXd & x
) {
	std::vector<glm::vec3> r;
	r.resize(x.rows());
	for(int i=0; i<x.rows(); i++) {
		r[i].x = x(i,0);
		r[i].y = x(i,1);
		r[i].z = x(i,2);
	}
	return r;
}

Eigen::MatrixXd toEigenMatrix(
	const std::vector<glm::vec3> & x
) {
	Eigen::MatrixXd r = Eigen::MatrixXd::Zero(x.size(),3);
	for(int i=0; i<x.size(); i++) {
		r(i,0) = x[i].x;
		r(i,1) = x[i].y;
		r(i,2) = x[i].z;
	}
	return r;
}

void Body::stepPD(float h, std::vector<Body*> & others) {
	if(isFixed) return;
	std::cout << "dt=" << h << std::endl;

	const int N = xs.size();

  Eigen::MatrixXd qn = Eigen::MatrixXd::Zero(N, 3);
  Eigen::MatrixXd vn = Eigen::MatrixXd::Zero(N, 3);
  Eigen::SparseMatrix<double> M(N, N);
	Eigen::SparseMatrix<double> Minv(N, N);
	Eigen::SparseMatrix<double> Msqrt(N, N);
  Eigen::MatrixXd Fext = Eigen::MatrixXd::Zero(N, 3);
  for(int i=0; i<N; i++) {
    // positions
    qn(i,0) = xs[i].x;
    qn(i,1) = xs[i].y;
    qn(i,2) = xs[i].z;
    // velocities
    vn(i,0) = vs[i].x;
    vn(i,1) = vs[i].y;
    vn(i,2) = vs[i].z;
    // masses
    M.insert(i,i) = ms[i];
		Minv.insert(i,i) = 1.f/ms[i];
		Msqrt.insert(i,i) = std::sqrt(ms[i]);
    // external forces
    Fext(i,0) = Fs[i].x;
    Fext(i,1) = Fs[i].y;
    Fext(i,2) = Fs[i].z;
  }

  Eigen::MatrixXd sn = Eigen::MatrixXd::Zero(N, 3);
  sn = qn + h*vn + h*h*Minv*Fext;

	Eigen::MatrixXd qn_1 = sn;

	for(auto & c : constraints) {
    if(c->getType()==ATTACH) {
      AttachConstraint* ac = (AttachConstraint*)c.get();
      sn(ac->i,0) = ac->p.x;
			sn(ac->i,1) = ac->p.y;
			sn(ac->i,2) = ac->p.z;
    }
  }

	// Generate collision constraints
	ps = toStdVector(sn);
  updateCollisionConstraints(others);

	const int C = constraints.size();
	std::cout << C << " constraints" << std::endl;

	// Constraints
	std::vector<Eigen::SparseMatrix<double>> Ss; // Constraints selection matrices
	std::vector<Eigen::SparseMatrix<double>> As; // Constraints A matrices
	std::vector<Eigen::SparseMatrix<double>> Bs; // Constraints B matrices
	std::vector<float> w; // Constraints weight (stiffnesses)
	Ss.resize(C, Eigen::SparseMatrix<double>(N,N));
	As.resize(C, Eigen::SparseMatrix<double>(N,N));
	Bs.resize(C, Eigen::SparseMatrix<double>(N,N));
	w.resize(C, 1);
	for(int j=0; j<C; j++) {
		auto & c = constraints.at(j);
		c->getSelectionMatrix(Ss.at(j));
		As[j].setIdentity();
		Bs[j].setIdentity();
		if(c->getType()==ATTACH) {
			w[j] = c->getStiffness();
			// As[j] = Ss[j] * (M);
			// Bs[j] = Ss[j] * (M);
		}
		else if(c->getType()==COLLISION) {
			w[j] = c->getStiffness();
			// As[j] = Ss[j] * (Minv);
			// Bs[j] = Ss[j] * (Minv);
		}
		else {
			w[j] = c->getStiffness();
			// As[j] = Ss[j] * (M);
			// Bs[j] = Ss[j] * (M);
		}
	}

	Eigen::SparseMatrix<double> Weight(N,N);
	Weight = (1.f/(h*h))*M;

	// The constant Omega
	Eigen::SparseMatrix<double> Omega(N,N);
	for(int j=0; j<C; j++) {
		Omega += w[j] * Ss[j].transpose()*As[j].transpose()*As[j]*Ss[j];
	}
	Eigen::JacobiSVD<Eigen::MatrixXd> svd(Omega, Eigen::ComputeThinU | Eigen::ComputeThinV);

	// Solving for q and p
	std::vector<Eigen::MatrixXd> ps;
	ps.resize(C, Eigen::MatrixXd::Zero(N,3));
	for(int i=0; i<nIters; i++) {
		std::cout << "iter: "<<i<<std::endl;

		// Local solves
		for(int j=0; j<C; j++) {
			auto constraint = constraints[j];
			auto qs = toStdVector(qn_1);
			constraint->pdLocalSolve(qs, pow(h,1));
			ps[j] = toEigenMatrix(qs);
		}

		// Global solve
		// Eigen::MatrixXd b = (1/(h*h))*M * sn;
		// for(int j=0; j<C; j++) {
		// 	b += w[j] * Ss[j].transpose() * As[j].transpose() * Bs[j] * ps[j];
		// }
		// qn_1 = svd.solve(b);

		Eigen::MatrixXd b = Eigen::MatrixXd::Zero(N,3);
		for(int j=0; j<C; j++) {
			b += w[j] * Ss[j].transpose() * As[j].transpose() * Bs[j] * (ps[j]-qn_1);
		}
		Eigen::MatrixXd delta_qn_1 = svd.solve(b);
		qn_1 = sn + delta_qn_1;
	}
	std::cout << std::endl;

	xs = toStdVector(qn_1);
	vs = toStdVector((1/h)*(qn_1-qn));
}

#ifndef _NUMERICAL_SOLVERS_
#define _NUMERICAL_SOLVERS_

#include <Eigen/Dense>

Eigen::MatrixXd solveConstrainedLeastSquares(
	const Eigen::MatrixXd & A, const Eigen::VectorXd & b,
	const Eigen::MatrixXd & B, const Eigen::VectorXd & d
) {
	const int N = A.cols();
	const int M1 = A.rows();
	const int M2 = B.rows();
	Eigen::MatrixXd LHS = Eigen::MatrixXd::Zero(N+M1+M2, N+M1+M2);
	Eigen::MatrixXd RHS = Eigen::MatrixXd::Zero(N+M1+M2, 1);

	LHS.block(0, N, N, M1) = A.transpose();
	LHS.block(0, N + M1, N, M2) = B.transpose();
	LHS.block(N, 0, M1, N) = A;
	LHS.block(N, N, M1, M1) = Eigen::MatrixXd::Identity(M1, M1);
	LHS.block(N + M1, 0, M2, N) = B;

	RHS.block(N, 0, M1, 1) = b;
	RHS.block(N + M1, 0, M2, 1) = d;

	Eigen::JacobiSVD<Eigen::MatrixXd> svd(LHS, Eigen::ComputeThinU | Eigen::ComputeThinV);

	return svd.solve(RHS);
}

#endif

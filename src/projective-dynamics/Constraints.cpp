#include "Constraints.hpp"


std::vector<glm::vec3> AttachConstraint::displace(
	std::vector<glm::vec3> & ps,
	const std::vector<float> & ms
) {
	glm::vec3 dp_ = ps[i]-p;
	glm::vec3 dp = float(1.0-glm::pow(1-k,1.f/n)) * dp_;
	ps[i] += dp;

	std::vector<glm::vec3> pdp;
	pdp.resize(ps.size(), glm::vec3(0));
	pdp[i] = dp_;
	return pdp;
}

void AttachConstraint::getSelectionMatrix(
	Eigen::SparseMatrix<double> & S
) {
	S.insert(i,i) = 1;
}

std::vector<glm::vec3> AttachConstraint::pdLocalSolve(
	std::vector<glm::vec3> & qs,
	float error
) {
	std::vector<float> ms; ms.resize(qs.size(), 1);
	return displace(qs, ms);
}









//------------------------------------------------
// Distance Constraints
//------------------------------------------------

std::vector<glm::vec3> DistanceConstraint::displace(
	std::vector<glm::vec3> & ps,
	const std::vector<float> & ws
) {
	float wi = ws[i];
	float wj = ws[j];

	glm::vec3 v = glm::normalize(ps[i]-ps[j]);
	float cd = glm::length(ps[i]-ps[j]);
	glm::vec3 dpi_ = -(wi/(wi+wj)) * (cd-d) * v;
	glm::vec3 dpj_ = (wj/(wi+wj)) * (cd-d) * v;

	glm::vec3 dpi = float(1.0-glm::pow(1-k,1.f/n)) * dpi_;
	glm::vec3 dpj = float(1.0-glm::pow(1-k,1.f/n)) * dpj_;

	ps[i] = ps[i] + dpi;
	ps[j] = ps[j] + dpj;

	std::vector<glm::vec3> pdp;
	pdp.resize(ps.size(), glm::vec3(0));
	pdp[i] = dpi_;
	pdp[j] = dpj_;
	return pdp;
}

void DistanceConstraint::generateConstraints(
	const std::vector<glm::vec3> & xs,
	std::vector<EdgeTriangles> & edges,
	float k,
	int n,
	std::vector<std::shared_ptr<Constraint>> & constraints
) {
	for(EdgeTriangles & edge : edges) {
		int i = edge.first[0];
		int j = edge.first[1];
		std::shared_ptr<Constraint> c = std::make_shared<DistanceConstraint>(
			i, j, glm::length(xs[i]-xs[j])
		);
		c->updateParams(k,n);
		constraints.push_back(c);
	}
}

void DistanceConstraint::getSelectionMatrix(
	Eigen::SparseMatrix<double> & S
) {
	S.insert(i,i) = 1;
	S.insert(j,j) = 1;
}

double distanceConstraintEnergyMetric(
	const std::vector<double> &x,
	std::vector<double> &grad,
	void ** data
) {
	DistanceConstraint * distanceConstraint = (DistanceConstraint*) data[0];
	glm::vec3 qi = * (glm::vec3*) data[1];
	glm::vec3 qj = * (glm::vec3*) data[2];
	float d = * (float*) data[3];

	Eigen::VectorXd q(6);
	q << qi.x, qi.y, qi.z, qj.x, qj.y, qj.z;

	Eigen::VectorXd p(6);
	glm::vec3 pi = {x[0],x[1],x[2]};
	glm::vec3 pj = {x[3],x[4],x[5]};
	p << pi.x, pi.y, pi.z, pj.x, pj.y, pj.z;

	double norm = (q-p).norm();
	// return gradient and value
	if(!grad.empty()) {
		Eigen::VectorXd gradient = (1/norm) * (q-p);
		if(norm==0) {
			grad[0] = 1e-7; grad[1] = 1e-7; grad[2] = 1e-7;
			grad[3] = 1e-7; grad[4] = 1e-7; grad[5] = 1e-7;
		}
		else {
			grad[0] = gradient(0); grad[1] = gradient(1); grad[2] = gradient(2);
			grad[3] = gradient(3); grad[4] = gradient(4); grad[5] = gradient(5);
		}
		if(
			grad[0]!=grad[0] || grad[1]!=grad[1] || grad[2]!=grad[2] ||
			grad[3]!=grad[3] || grad[4]!=grad[4] || grad[5]!=grad[5]
		) {
			// assert(false && "distance metric gradient is nan");
			grad[0] = 0; grad[1] = 0; grad[2] = 0;
			grad[3] = 0; grad[4] = 0; grad[5] = 0;
		}
	}
	if(norm!=norm) {
		// assert(false && "distance metric is nan");
		return 0;
	}
	return norm;
}
double distanceConstraintManifold(
	const std::vector<double> &x,
	std::vector<double> &grad,
	void ** data
) {
	DistanceConstraint * distanceConstraint = (DistanceConstraint*) data[0];
	glm::vec3 qi = * (glm::vec3*) data[1];
	glm::vec3 qj = * (glm::vec3*) data[2];
	float d0 = * (float*) data[3];
	// P position matrix to vector
	glm::vec3 pi = {x[0],x[1],x[2]};
	glm::vec3 pj = {x[3],x[4],x[5]};
	glm::vec3 n = glm::normalize(pi-pj);
	float d = glm::length(pi-pj);
	if(d==0) n = glm::normalize(glm::vec3(1))*(float)1e-7;
	// Gradient
	if(!grad.empty()) {
		n = n * (d-d0);
		grad[0] = n.x; grad[1] = n.y; grad[2] = n.z;
		grad[3] = -n.x; grad[4] = -n.y; grad[5] = -n.z;
		if(n.x!=n.x || n.y!=n.y || n.z!=n.z) {
			// assert(false && "distance constraint n is nan");
			grad[0] = 0; grad[1] = 0; grad[2] = 0;
			grad[3] = 0; grad[4] = 0; grad[5] = 0;
		}
	}
	if(d!=d) {
		// assert(false && "distance constraint d is nan");
		return 0;
	}
	// Value
	return d-d0;
}
std::vector<glm::vec3> DistanceConstraint::pdLocalSolve(
	std::vector<glm::vec3> & qs,
  float error
) {
	std::vector<float> ms; ms.resize(qs.size(), 1);
	return displace(qs, ms);


	nlopt::opt opt_x(nlopt::LD_SLSQP, 6);
	void * data[] = {this, (void*)&qs.at(i), (void*)&qs.at(j), &d};

	opt_x.set_min_objective((nlopt::vfunc)distanceConstraintEnergyMetric, data);

	opt_x.add_equality_constraint((nlopt::vfunc)distanceConstraintManifold, data);
	opt_x.set_ftol_abs(1e-7);
	opt_x.set_maxeval(1000);

	std::vector<double> vecQ(6);
	vecQ[0] = qs[i].x; vecQ[1] = qs[i].y; vecQ[2] = qs[i].z;
	vecQ[3] = qs[j].x; vecQ[4] = qs[j].y; vecQ[5] = qs[j].z;
	double minEnergy=0;

	std::vector<glm::vec3> q = qs;
	try{
		nlopt::result result = opt_x.optimize(vecQ, minEnergy);
		qs[i] = glm::vec3(vecQ[0],vecQ[1],vecQ[2]);
		qs[j] = glm::vec3(vecQ[3],vecQ[4],vecQ[5]);
		q[i] = qs[i] - q[i];
		q[j] = qs[j] - q[j];
	}
	catch(std::exception &e) {
	}
	return q;
}



















//------------------------------------------------
// Bending Constraints
//------------------------------------------------

std::vector<glm::vec3> BendingConstraint::displace(
	std::vector<glm::vec3> & ps,
	const std::vector<float> & ws
) {
	const float ZR = 0.0001f;
	float w1 = ws[i1];
	float w2 = ws[i2];
	float w3 = ws[i3];
	float w4 = ws[i4];
	glm::vec3 p1 = ps[i1];
	glm::vec3 p2 = ps[i2];
	glm::vec3 p3 = ps[i3];
	glm::vec3 p4 = ps[i4];
	p2 = p2-p1;
	p3 = p3-p1;
	p4 = p4-p1;
	p1 = glm::vec3(0);
	// Normal of face i1,i2,i3 and i1,i2,i4
	glm::vec3 n1 = glm::normalize(glm::cross(p2,p3));
	glm::vec3 n2 = glm::normalize(glm::cross(p2,p4));
	float d = glm::dot(n1,n2);
	if(d<-1) d=-1;
	else if(d>1) d=1;

	glm::vec3 q3 = (1.0f/(glm::length(glm::cross(p2,p3))+ZR)) * (glm::cross(p2,n2) + glm::cross(n1,p2)*d);
	glm::vec3 q4 = (1.0f/(glm::length(glm::cross(p2,p4))+ZR)) * (glm::cross(p2,n1) + glm::cross(n2,p2)*d);
	glm::vec3 q2 = 
		-(1.0f/(glm::length(glm::cross(p2,p3))+ZR)) * (glm::cross(p3,n2) + glm::cross(n1,p3)*d)
		-(1.0f/(glm::length(glm::cross(p2,p4))+ZR)) * (glm::cross(p4,n1) + glm::cross(n2,p4)*d);
	glm::vec3 q1 = -q2-q3-q4;

	float s = 
		w1*std::pow(glm::length(q1),2) +
		w2*std::pow(glm::length(q2),2) +
		w3*std::pow(glm::length(q3),2) +
		w4*std::pow(glm::length(q4),2);
	glm::vec3 dp1_ = -(1.0f/(s+ZR))*(w1*std::sqrt(1-d*d)*(std::acos(d)-phi))*q1;
	glm::vec3 dp2_ = -(1.0f/(s+ZR))*(w2*std::sqrt(1-d*d)*(std::acos(d)-phi))*q2;
	glm::vec3 dp3_ = -(1.0f/(s+ZR))*(w3*std::sqrt(1-d*d)*(std::acos(d)-phi))*q3;
	glm::vec3 dp4_ = -(1.0f/(s+ZR))*(w4*std::sqrt(1-d*d)*(std::acos(d)-phi))*q4;
	
	glm::vec3 dp1 = float(1.0-glm::pow(1-k,1.f/n)) * dp1_;
	glm::vec3 dp2 = float(1.0-glm::pow(1-k,1.f/n)) * dp2_;
	glm::vec3 dp3 = float(1.0-glm::pow(1-k,1.f/n)) * dp3_;
	glm::vec3 dp4 = float(1.0-glm::pow(1-k,1.f/n)) * dp4_;

	ps[i1] = ps[i1] + dp1;
	ps[i2] = ps[i2] + dp2;
	ps[i3] = ps[i3] + dp3;
	ps[i4] = ps[i4] + dp4;

	std::vector<glm::vec3> pdp;
	pdp.resize(ps.size(), glm::vec3(0));
	pdp[i1] = dp1_;
	pdp[i2] = dp2_;
	pdp[i3] = dp3_;
	pdp[i4] = dp4_;
	return pdp;
}

void BendingConstraint::generateConstraints(
	const std::vector<glm::vec3> & xs,
	std::vector<EdgeTriangles> & edges,
	float k,
	int n,
	std::vector<std::shared_ptr<Constraint>> & constraints
) {
	for(EdgeTriangles & edge : edges) {
		int i1 = edge.first[0];
		int i2 = edge.first[1];
		int i3 = edge.second[0];
		int i4 = edge.second[1];
		if(i3>=0 && i4>=0) {
			glm::vec3 p1 = xs[i1];
			glm::vec3 p2 = xs[i2];
			glm::vec3 p3 = xs[i3];
			glm::vec3 p4 = xs[i4];
			p2 = p2-p1; p3 = p3-p1; p4 = p4-p1;
			// Normal of face i1,i2,i3 and i1,i2,i4
			glm::vec3 n1 = glm::normalize(glm::cross(p2,p3));
			glm::vec3 n2 = glm::normalize(glm::cross(p2,p4));
			float phi = glm::acos(glm::max(glm::dot(n1,n2),-0.9999f));
			std::shared_ptr<Constraint> c = std::make_shared<BendingConstraint>(
				i1,i2,i3,i4,phi
			);
			c->updateParams(k,n);
			constraints.push_back(c);
		}
	}
}

void BendingConstraint::getSelectionMatrix(
	Eigen::SparseMatrix<double> & S
) {
	S.insert(i1,i1) = 1;
	S.insert(i2,i2) = 1;
	S.insert(i3,i3) = 1;
	S.insert(i4,i4) = 1;
}


double bendingConstraintEnergyMetric(
	const std::vector<double> &x,
	std::vector<double> &grad,
	void ** data
) {
	BendingConstraint * bendingConstraint = (BendingConstraint*) data[0];
	glm::vec3 qi1 = * (glm::vec3*) data[1];
	glm::vec3 qi2 = * (glm::vec3*) data[2];
	glm::vec3 qi3 = * (glm::vec3*) data[3];
	glm::vec3 qi4 = * (glm::vec3*) data[4];
	float phi0 = * (float*) data[5];

	Eigen::VectorXd q(12);
	q << 
	qi1.x, qi1.y, qi1.z,
	qi2.x, qi2.y, qi2.z,
	qi3.x, qi3.y, qi3.z,
	qi4.x, qi4.y, qi4.z;

	Eigen::VectorXd p(12);
	glm::vec3 pi1 = {x[0],x[1],x[2]};
	glm::vec3 pi2 = {x[3],x[4],x[5]};
	glm::vec3 pi3 = {x[6],x[7],x[8]};
	glm::vec3 pi4 = {x[9],x[10],x[11]};
	p <<
	pi1.x, pi1.y, pi1.z,
	pi2.x, pi2.y, pi2.z,
	pi3.x, pi3.y, pi3.z,
	pi4.x, pi4.y, pi4.z;

	double norm = (q-p).norm();
	if(norm != norm) {
		// assert(false && "bending constraint norm is nan");
		return 0;
	}
	// return gradient and value
	if(!grad.empty()) {
		Eigen::VectorXd gradient = (1/norm) * (q-p);
		if(norm==0) {
			grad[0] = 0; grad[1] = 0; grad[2] = 0;
			grad[3] = 0; grad[4] = 0; grad[5] = 0;
			grad[6] = 0; grad[7] = 0; grad[8] = 0;
			grad[9] = 0; grad[10] = 0; grad[11] = 0;
		}
		else {
			grad[0] = gradient(0); grad[1] = gradient(1); grad[2] = gradient(2);
			grad[3] = gradient(3); grad[4] = gradient(4); grad[5] = gradient(5);
			grad[6] = gradient(6); grad[7] = gradient(7); grad[8] = gradient(8);
			grad[9] = gradient(9); grad[10] = gradient(10); grad[11] = gradient(1);
		}
	}
	return norm;
}
double bendingConstraintManifold(
	const std::vector<double> &x,
	std::vector<double> &grad,
	void ** data
) {
	BendingConstraint * bendingConstraint = (BendingConstraint*) data[0];
	glm::vec3 qi1 = * (glm::vec3*) data[1];
	glm::vec3 qi2 = * (glm::vec3*) data[2];
	glm::vec3 qi3 = * (glm::vec3*) data[3];
	glm::vec3 qi4 = * (glm::vec3*) data[4];
	float phi0 = * (float*) data[5];

	glm::vec3 pi1 = {x[0],x[1],x[2]};
	glm::vec3 pi2 = {x[3],x[4],x[5]};
	glm::vec3 pi3 = {x[6],x[7],x[8]};
	glm::vec3 pi4 = {x[9],x[10],x[11]};

	pi2 -= pi1; pi3 -= pi1; pi4 -= pi1;
	glm::vec3 n1 = glm::normalize(glm::cross(pi2,pi3));
	glm::vec3 n2 = glm::normalize(glm::cross(pi2,pi4));
	float d = glm::max(glm::dot(n1,n2),-0.9999f);
	float phi = glm::acos(d);

	if(!grad.empty()) {
		float co = -1/(sqrt(1-d*d)+(float)1e-6);
		glm::vec3 grad_pi3 = co * (1/(glm::length(glm::cross(pi2,pi3))+(float)1e-6)) * (glm::cross(pi2,n2)+d*glm::cross(n1,pi2));
		glm::vec3 grad_pi4 = co * (1/(glm::length(glm::cross(pi2,pi4))+(float)1e-6)) * (glm::cross(pi2,n1)+d*glm::cross(n2,pi2));
		glm::vec3 grad_pi2 = co *(
			-(1/(glm::length(glm::cross(pi2,pi3))+(float)1e-6)) * (glm::cross(pi3,n2)+d*glm::cross(n1,pi3))
			-(1/(glm::length(glm::cross(pi2,pi4))+(float)1e-6)) * (glm::cross(pi4,n1)+d*glm::cross(n2,pi4))
		);
		glm::vec3 grad_pi1 = -grad_pi2-grad_pi3-grad_pi4;
		// grad_pi3 *= (phi-phi0);
		// grad_pi4 *= (phi-phi0);
		// grad_pi2 *= (phi-phi0);
		// grad_pi1 *= (phi-phi0);
		grad[0] = grad_pi1.x; grad[1] = grad_pi1.y; grad[2] = grad_pi1.z;
		grad[3] = grad_pi2.x; grad[4] = grad_pi2.y; grad[5] = grad_pi2.z;
		grad[6] = grad_pi3.x; grad[7] = grad_pi3.y; grad[8] = grad_pi3.z;
		grad[9] = grad_pi4.x; grad[10] = grad_pi4.y; grad[11] = grad_pi4.z;
		if(grad_pi1.x!=grad_pi1.x || grad_pi1.y!=grad_pi1.y || grad_pi1.z!=grad_pi1.z) {
			// assert(false && "bending constraint grad is nan");
			for(int i=0; i<12; i++) grad[i] = 0;
		}
	}
	if(phi!=phi) {
		// assert(false && "bending constraint phi is nan");
		return 0;
	}

	return phi-phi0;
}
std::vector<glm::vec3> BendingConstraint::pdLocalSolve(
	std::vector<glm::vec3> & qs,
    float error
) {
	std::vector<float> ms; ms.resize(qs.size(), 1);
	return displace(qs, ms);


	nlopt::opt opt_x(nlopt::LD_SLSQP, 12);
	void * data[] = {
		this, (void*)&qs.at(i1), (void*)&qs.at(i2), (void*)&qs.at(i3), (void*)&qs.at(i4), &phi
	};

	opt_x.set_min_objective((nlopt::vfunc)bendingConstraintEnergyMetric, data);

	opt_x.add_equality_constraint((nlopt::vfunc)bendingConstraintManifold, data);
	opt_x.set_ftol_abs(DBL_MIN);

	std::vector<double> vecQ(12);
	vecQ[0] = qs[i1].x; vecQ[1] = qs[i1].y; vecQ[2] = qs[i1].z;
	vecQ[3] = qs[i2].x; vecQ[4] = qs[i2].y; vecQ[5] = qs[i2].z;
	vecQ[6] = qs[i3].x; vecQ[7] = qs[i3].y; vecQ[8] = qs[i3].z;
	vecQ[9] = qs[i4].x; vecQ[10] = qs[i4].y; vecQ[11] = qs[i4].z;
	double minEnergy=0;

	std::vector<glm::vec3> q = qs;
	try{
		nlopt::result result = opt_x.optimize(vecQ, minEnergy);
		qs[i1] = glm::vec3(vecQ[0],vecQ[1],vecQ[2]);
		qs[i2] = glm::vec3(vecQ[3],vecQ[4],vecQ[5]);
		qs[i3] = glm::vec3(vecQ[6],vecQ[7],vecQ[8]);
		qs[i4] = glm::vec3(vecQ[9],vecQ[10],vecQ[11]);
		q[i1] = qs[i1] - q[i1];
		q[i2] = qs[i2] - q[i2];
		q[i3] = qs[i3] - q[i3];
		q[i4] = qs[i4] - q[i4];
	}
	catch(std::exception &e) {
	}
	return q;
}

















//------------------------------------------------
// Collision Constraints
//------------------------------------------------

std::vector<glm::vec3> CollisionConstraint::displace(
	std::vector<glm::vec3> & ps,
	const std::vector<float> & ws
) {
	auto eval = evaluate(ps);
	glm::vec3 dp_ = {0,0,0};
	if(eval<0) {
		// (>=0) Inequality constraint
		dp_ = -eval*normal;
		glm::vec3 dp = float(1.0-glm::pow(1-k,1.f/n)) * dp_;
		ps[pIndex] = ps[pIndex] + dp;
	}

	std::vector<glm::vec3> pdp;
	pdp.resize(ps.size(), glm::vec3(0));
	pdp[pIndex] = dp_;
	return pdp;
}

float CollisionConstraint::evaluate(
	const std::vector<glm::vec3> & ps
) {
	if(isStatic) {
		return glm::dot(ps[pIndex]-interPoint, normal);
	}
	else {
		return glm::dot(ps[pIndex]-p1, glm::cross(p2-p1,p3-p1));
	}
}

bool CollisionConstraint::detectRayIntersection(
	const glm::vec3 & rayStart, const glm::vec3 & rayDirection,
	const std::vector<glm::vec3> & bodyVertices,
	const std::vector<glm::uvec3> & bodyTriangles,
	glm::uvec3 & intersectionTriangle,
	glm::vec3 & intersectionPoint,
	float & intersectionDistanceFromX
) {
	float minDist = FLT_MAX;
	for(auto & t : bodyTriangles) {
		glm::vec3 interPoint;
		bool inter = rayTriangleIntersection(
			bodyVertices.at(t[0]),
			bodyVertices.at(t[1]),
			bodyVertices.at(t[2]),
			rayStart, rayDirection, interPoint
		);
		if(inter && glm::dot(rayDirection, interPoint-rayStart)>=0) {
			float dist = glm::length(interPoint-rayStart);
			if(dist<minDist) {
				intersectionPoint = interPoint;
				intersectionTriangle = t;
				intersectionDistanceFromX = dist;
				minDist = dist;
			}
		}
	}
	return minDist != FLT_MAX;
}

bool CollisionConstraint::detectCollision(
	const glm::vec3 & x, const glm::vec3 & p,
	const std::vector<glm::vec3> & bodyVertices,
	const std::vector<glm::uvec3> & bodyTriangles,
	CollisionInfo & info
) {
	glm::uvec3 forwardCollTriangle;
	glm::vec3 forwardCollPoint;
	float forwardCollDistance;
	bool forwardCollision = detectRayIntersection(
		x, p-x, bodyVertices, bodyTriangles,
		forwardCollTriangle, forwardCollPoint, forwardCollDistance
	);

	glm::uvec3 backwardCollTriangle;
	glm::vec3 backwardCollPoint;
	float backwardCollDistance;
	bool backwardCollision = detectRayIntersection(
		x, x-p, bodyVertices, bodyTriangles,
		backwardCollTriangle, backwardCollPoint, backwardCollDistance
	);
	
	if(
		forwardCollision && backwardCollision
	) {
		info.isInside = true;
		if(forwardCollDistance>backwardCollDistance) {
			info.triangle = backwardCollTriangle;
			info.point = backwardCollPoint;
			info.distanceFromX = backwardCollDistance;
		}
		else {
			info.triangle = forwardCollTriangle;
			info.point = forwardCollPoint;
			info.distanceFromX = forwardCollDistance;
		}
		return true;
	}

	if(forwardCollision) {
		if(forwardCollDistance > glm::length(p-x)) {
			// Collision too far
			return false;
		}
		else {
			// Collision will happen
			info.isInside = false;
			info.triangle = forwardCollTriangle;
			info.point = forwardCollPoint;
			info.distanceFromX = forwardCollDistance;
		}
	}
	return false;
}

void CollisionConstraint::generateConstraints(
	std::string & name,
	const std::vector<glm::vec3> & xs,
	const std::vector<glm::vec3> & ps,
	std::vector<CollisionConstraint::BodyInfo> & otherBodies,
	float k,
	int n,
	std::vector<std::shared_ptr<Constraint>> & constraints
) {
	for(unsigned int i=0; i<xs.size(); i++) {
		auto & x = xs[i];
		auto & p = ps[i];

		CollisionConstraint::CollisionInfo closest;
		closest.distanceFromX = FLT_MAX;
		unsigned int body = 0;
		for(unsigned int j=0; j<otherBodies.size(); j++) {
			CollisionConstraint::CollisionInfo info;
			if(detectCollision(x, p, *otherBodies[j].xs, *otherBodies[j].triangles, info)) {
				if(info.isInside) {
					closest = info;
					body = j;
					break;
				}
				else {
					if(info.distanceFromX<closest.distanceFromX) {
						closest = info;
						body = j;
					}
				}
			}
		}

		if(closest.distanceFromX != FLT_MAX) {
			auto p1 = otherBodies.at(body).xs->at(closest.triangle[0]);
			auto p2 = otherBodies.at(body).xs->at(closest.triangle[1]);
			auto p3 = otherBodies.at(body).xs->at(closest.triangle[2]);

			glm::vec normal = glm::normalize(glm::cross(
				p3-p1, p2-p1
			));
			if(glm::dot(normal,p-x)>0)
				normal *= -1;

			std::shared_ptr<Constraint> constraint = std::make_shared<CollisionConstraint>(
				i, closest.point, normal, p1, p2, p3, otherBodies.at(body).isStatic
			);
			constraint->updateParams(k, n);

			// std::cout << name << " to " << otherBodies[body].name << std::endl;
			// std::cout << "\t x=" << x << " p=" << p << std::endl;
			// std::cout << "\t collision point=" << closest.point << std::endl;
			// std::cout << "\t dist(x-p)=" << glm::length(x-p) << std::endl;
			// std::cout << "\t collision dist=" << closest.distanceFromX << std::endl;
			// std::cout << "\t p1=" << p1 << " p2=" << p2 << " p3=" << p3 << std::endl;

			std::cout << "Collision " << (closest.isInside?"inside":"") << std::endl;

			constraints.push_back(constraint);
		}
	}
}

void CollisionConstraint::getSelectionMatrix(
	Eigen::SparseMatrix<double> & S
) {
	S.insert(pIndex,pIndex) = 1;
}


double collisionConstraintEnergyMetric(
	const std::vector<double> &x,
	std::vector<double> &grad,
	void ** data
) {
	CollisionConstraint * collisionConstraint = (CollisionConstraint*) data[0];
	glm::vec3 qc = * (glm::vec3*) data[1];

	Eigen::VectorXd q(3);
	q << 
	qc.x, qc.y, qc.z;

	Eigen::VectorXd p(3);
	glm::vec3 pc = {x[0],x[1],x[2]};
	p <<
	pc.x, pc.y, pc.z;

	double norm = (q-p).norm();
	// return gradient and value
	if(!grad.empty()) {
		Eigen::VectorXd gradient = (1/norm) * (q-p);
		if(norm==0) {
			grad[0] = 0; grad[1] = 0; grad[2] = 0;
		}
		else {
			grad[0] = gradient(0); grad[1] = gradient(1); grad[2] = gradient(2);
		}
	}
	if(norm != norm) {
		// assert(false && "collision constraint norm is nan");
		return 0;
	}
	return norm;
}
double collisionConstraintManifold(
	const std::vector<double> &x,
	std::vector<double> &grad,
	void ** data
) {
	CollisionConstraint * collisionConstraint = (CollisionConstraint*) data[0];
	glm::vec3 qc = * (glm::vec3*) data[1];
	glm::vec3 interPoint = * (glm::vec3*) data[2];
  glm::vec3 normal = * (glm::vec3*) data[3];

	glm::vec3 pc = {x[0],x[1],x[2]};

	float v = glm::dot(pc-interPoint, normal);

	if(!grad.empty()) {
		glm::vec3 grad_v = normal;
		grad_v = v*grad_v;
		grad[0] = grad_v.x; grad[1] = grad_v.y; grad[2] = grad_v.z;
	}
	return -v;
}
std::vector<glm::vec3> CollisionConstraint::pdLocalSolve(
	std::vector<glm::vec3> & qs,
    float error
) {
	std::vector<float> ms; ms.resize(qs.size(), 1);
	std::cout << "Before: " << qs[pIndex] << " eval ---> " << evaluate(qs) << std::endl;
	auto qq =  displace(qs, ms);
	std::cout << "After: " << qs[pIndex] << " eval ---> " << evaluate(qs) << std::endl;
	return qq;

	nlopt::opt opt_x(nlopt::LD_MMA, 3);
	void * data[] = {
		this, (void*)&qs.at(pIndex), &interPoint, &normal
	};

	opt_x.set_min_objective((nlopt::vfunc)collisionConstraintEnergyMetric, data);

	opt_x.add_inequality_constraint((nlopt::vfunc)collisionConstraintManifold, data);
	opt_x.set_ftol_abs(DBL_MIN);


	std::vector<double> vecQ(3);
	vecQ[0] = qs[pIndex].x; vecQ[1] = qs[pIndex].y; vecQ[2] = qs[pIndex].z;
	double minEnergy=0;

	std::vector<glm::vec3> q = qs;
	try{
		nlopt::result result = opt_x.optimize(vecQ, minEnergy);

		std::cout << "Before: " << qs[pIndex] << " eval ---> " << evaluate(qs) << std::endl;

		qs[pIndex] = glm::vec3(vecQ[0],vecQ[1],vecQ[2]);
		q[pIndex] = qs[pIndex] - q[pIndex];

		std::cout << "Before: " << qs[pIndex] << " eval ---> " << evaluate(qs) << std::endl;
	}
	catch(std::exception &e) {
	}
	return q;
}

#ifndef _PROJECTIVE_DYNAMICS_CONSTRAINTS_
#define _PROJECTIVE_DYNAMICS_CONSTRAINTS_

#include "../Utils.hpp"
#include "Geometry.hpp"

// For analytical solvers
#include <Eigen/Dense>
#include <Eigen/Sparse>

// For iterative solvers
#include <nlopt.hpp>

enum ConstraintType {
  DISTANCE, BENDING, COLLISION, ATTACH
};

class Constraint {
public:
  virtual ConstraintType getType() const = 0;
  virtual std::vector<glm::vec3> displace(
    std::vector<glm::vec3> & ps,
    const std::vector<float> & ms
  ) {return {};}

  inline void updateParams(float k, float n) {
    this->k = k;
    this->n = n;
  }

  virtual void getSelectionMatrix(
    Eigen::SparseMatrix<double> & S
  ) {}

  virtual std::vector<glm::vec3> pdLocalSolve(
    std::vector<glm::vec3> & qs,
    float error
  ) {}

  inline float getStiffness() const {return k;}

protected:
  // Stiffness
  float k = 0;
  // Stiffness power
  int n = 0;
};


//------------------------------------------------
// Attach Constraints
//------------------------------------------------

class AttachConstraint: public Constraint {
public:
  AttachConstraint(
    glm::vec3 & p, int i
  )
  : p(p), i(i) {}

  inline ConstraintType getType() const override {
    return ConstraintType::ATTACH;
  }
  glm::vec3 p;
  int i;
private:

public:
  std::vector<glm::vec3> displace(
    std::vector<glm::vec3> & ps,
    const std::vector<float> & ms
  ) override;

  void getSelectionMatrix(
    Eigen::SparseMatrix<double> & S
  ) override;

  std::vector<glm::vec3> pdLocalSolve(
    std::vector<glm::vec3> & qs,
    float error
  ) override;
};


//------------------------------------------------
// Distance Constraints
//------------------------------------------------

class DistanceConstraint : public Constraint {
public:
  DistanceConstraint(
    int i, int j, float d
  )
  : i(i), j(j), d(d) {}

  inline ConstraintType getType() const override {
    return ConstraintType::DISTANCE;
  }

private:
  // positions indices
  int i=0, j=0;
  // Initial length
  float d = 0;

public:
  std::vector<glm::vec3> displace(
    std::vector<glm::vec3> & ps,
    const std::vector<float> & ms
  ) override;

  static void generateConstraints(
    const std::vector<glm::vec3> & xs,
    std::vector<EdgeTriangles> & edges,
    float k,
    int n,
    std::vector<std::shared_ptr<Constraint>> & constraints
  );

  void getSelectionMatrix(
    Eigen::SparseMatrix<double> & S
  ) override;

  std::vector<glm::vec3> pdLocalSolve(
    std::vector<glm::vec3> & qs,
    float error
  ) override;
};


//------------------------------------------------
// Bending Constraints
//------------------------------------------------

class BendingConstraint : public Constraint {
public:
  BendingConstraint(
    int i1, int i2, int i3, int i4, float phi
  )
  : i1(i1), i2(i2), i3(i3), i4(i4), phi(phi) {}

  inline ConstraintType getType() const override {
    return ConstraintType::BENDING;
  }

private:
  // faces i1,i2,i3 and i1,i2,i4
  int i1=0, i2=0, i3=0, i4=0;
  // Initial angle
  float phi=0;

public:
  std::vector<glm::vec3> displace(
    std::vector<glm::vec3> & ps,
    const std::vector<float> & ms
  ) override;
  
  static void generateConstraints(
    const std::vector<glm::vec3> & xs,
    std::vector<EdgeTriangles> & edges,
    float k,
    int n,
    std::vector<std::shared_ptr<Constraint>> & constraints
  );

  void getSelectionMatrix(
    Eigen::SparseMatrix<double> & S
  ) override;

  std::vector<glm::vec3> pdLocalSolve(
    std::vector<glm::vec3> & qs,
    float error
  ) override;
};

//------------------------------------------------
// Collision Constraints
//------------------------------------------------

class CollisionConstraint : public Constraint {
public:
  CollisionConstraint(
    unsigned int pIndex,
    glm::vec3 & interPoint,
    glm::vec3 & normal,
    glm::vec3 & p1, glm::vec3 & p2, glm::vec3 & p3,
    bool isStatic
  )
  : pIndex(pIndex),interPoint(interPoint),
  normal(normal), p1(p1),p2(p2),p3(p3),
  isStatic(isStatic) {}

  inline ConstraintType getType() const override {
    return ConstraintType::COLLISION;
  }

private:
  unsigned int pIndex=0;

  glm::vec3 interPoint;

  glm::vec3 normal;
  glm::vec3 p1, p2, p3;

  bool isStatic;

public:
  struct BodyInfo {
    std::vector<glm::vec3> * xs;
    std::vector<glm::uvec3> * triangles;
    bool isStatic;
    std::string name;

  };

  std::vector<glm::vec3> displace(
    std::vector<glm::vec3> & ps,
    const std::vector<float> & ms
  ) override;

private:
  float evaluate(
    const std::vector<glm::vec3> & ps
  );

  /*Detect intersection of ray with the body.*/
  static bool detectRayIntersection(
    const glm::vec3 & rayStart, const glm::vec3 & rayDirection,
    const std::vector<glm::vec3> & bodyVertices,
    const std::vector<glm::uvec3> & bodyTriangles,
    glm::uvec3 & intersectionTriangle,
    glm::vec3 & intersectionPoint,
    float & intersectionDistanceFromX
  );

  struct CollisionInfo {
    glm::uvec3 triangle;
    glm::vec3 normal;
    glm::vec3 point;
    float distanceFromX;
    bool isInside;
    bool isback;
  };

  static bool detectCollision(
    const glm::vec3 & x, const glm::vec3 & p,
    const std::vector<glm::vec3> & bodyVertices,
    const std::vector<glm::uvec3> & bodyTriangles,
    CollisionInfo & info
  );

public:
  static void generateConstraints(
    std::string & name,
    const std::vector<glm::vec3> & xs,
    const std::vector<glm::vec3> & ps,
    std::vector<CollisionConstraint::BodyInfo> & otherBodies,
    float k,
    int n,
    std::vector<std::shared_ptr<Constraint>> & constraints
  );

  void getSelectionMatrix(
    Eigen::SparseMatrix<double> & S
  ) override;

  std::vector<glm::vec3> pdLocalSolve(
    std::vector<glm::vec3> & qs,
    float error
  ) override;
};

#endif

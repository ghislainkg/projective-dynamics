#ifndef _PROJECTIVE_DYNAMICS_BODY_
#define _PROJECTIVE_DYNAMICS_BODY_

#include "../Utils.hpp"
#include "Geometry.hpp"
#include "Constraints.hpp"

#define EARTH_G 9.807

enum DynamicMethod {
  PBD, PROJECTIVE_DYNAMICS
};

class Body {
public:
  Body(
    const std::vector<glm::vec3> & vertices,
    const std::vector<float> & mass,
    const std::vector<glm::uvec3> & triangles
  ): vertices(vertices), ms(mass), triangles(triangles) {}

  void init();
  void step(float dt, std::vector<Body*> & others);

  std::vector<glm::vec3> & getParticlesPos() {return xs;}
  std::vector<glm::uvec3> & getTriangles() {return triangles;}

  bool isFixed = false;

  std::vector<glm::vec3> vertices; // Particles initial positions
  std::vector<glm::uvec3> triangles;

  DynamicMethod method = DynamicMethod::PROJECTIVE_DYNAMICS;

private:
  std::vector<EdgeTriangles> edges;

  std::vector<float> ms; // Particles mass
  std::vector<glm::vec3> xs; // Particles current positions
  std::vector<glm::vec3> ps; // Particles unconstrained positions
  std::vector<glm::vec3> vs; // Particles current velocities
  std::vector<glm::vec3> Fs; // Particles external forces

public:
  int nIters = 30; // Constraints projection solve iterations

  float distanceConstraintStiffness = 0.5;
  float bendingConstraintStiffness = 0.5;
  float collisionConstraintStiffness = 1;

  std::string name = "";

private:
  std::vector<std::shared_ptr<Constraint>> constraints;

  void updateCollisionConstraints(std::vector<Body*> & others);
  void updateConstraintsParams();

  void stepPBD(float dt, std::vector<Body*> & others);

  void stepPD(float dt, std::vector<Body*> & others);

public:
  inline void addConstraint(std::shared_ptr<Constraint> c) {
    constraints.push_back(c);
  }
  inline void removeConstraint(std::shared_ptr<Constraint> c) {
    std::remove_if(
      constraints.begin(), constraints.end(),
      [&](std::shared_ptr<Constraint> b) {
        return b.get()==c.get();
      });
  }
};

#endif

#include "DynamicEngine.hpp"

class SimulationPanel : public gumrender3d::PanelImGui {
public:
  SimulationPanel() {
    this->name = "Simulation";
  }
  DynamicEngine * engine = nullptr;
  void render() override {
    if(engine) {
      ImGui::Checkbox("Simulate##dynamicenginesimulate", &engine->sim);
      ImGui::InputInt("Skip frames##dynamicengineskipframes", &engine->skipStepCount);
      ImGui::InputInt("Delta fraction##dynamicenginedeltafraction", &engine->deltaFrac);
      ImGui::InputInt("n iters##dynamicengineniters", &engine->niters, 1, 5);
      if(ImGui::RadioButton("PBD", engine->method==DynamicMethod::PBD)) {
        engine->method=DynamicMethod::PBD;
      }
      if(ImGui::RadioButton("Projective D", engine->method==DynamicMethod::PROJECTIVE_DYNAMICS)) {
        engine->method=DynamicMethod::PROJECTIVE_DYNAMICS;
      }
    }
  }
};

class DynamicBodyPanel : public gumrender3d::PanelImGui {
public:
  DynamicBodyPanel() {
    this->name = "Dynamic body";
  }
  Body * body = nullptr;
  void render() override {
    if(body) {
      ImGui::Checkbox("Is fixed##dynamicbodyisfixed", &body->isFixed);
      ImGui::InputFloat("distance constraint stiffness", &body->distanceConstraintStiffness, 0.01f, 0.1f);
      ImGui::InputFloat("bending constraint stiffness", &body->bendingConstraintStiffness, 0.01f, 0.1f);
      ImGui::InputFloat("collision constraint stiffness", &body->collisionConstraintStiffness, 0.01f, 0.1f);
    }
  }
};

class AppWindow : public gumrender3d::Window3D {
public:
  AppWindow(): Window3D(900, 700, "Projective Dynamics") {}

  // Renderer
  gumrender3d::Renderer renderer;
  // Camera controller
  gumrender3d::MouseFocusController controller;
  // ImGui Panels handler
  gumrender3d::PanelsAppendImGui panelsImGui;
  // Frame capture
  gumrender3d::ColorCapture colorCapture;
  bool pendingCapture = false;
  int captureID = 0;

  DynamicEngine engine;

  // ImGui panels
  gumrender3d::CameraPanelImGui cameraPanel;
  SimulationPanel simulationPanel;
  gumrender3d::MeshSelectorPanelImGui<std::vector> meshSelectorPanel;
  gumrender3d::MeshDataViewPanelImGui meshDataViewPanel;
  gumrender3d::MeshDataTransformPanelImGui meshDataTransformPanel;
  gumrender3d::MeshShadingPanelImGui meshShadingPanel;
  DynamicBodyPanel dynamicBodyPanel;

protected:

  inline void afterInit() override {
    renderer.init();
    // Environment light
    renderer.light.intensity = 1.0f;
    renderer.light.ambient = 0.6f;
    // Camera and camera control
    renderer.camera.position = {8, 9, -2};
    controller = gumrender3d::MouseFocusController(width, height, &renderer.camera);

    // ImGui panels
    panelsImGui = gumrender3d::PanelsAppendImGui(window, "Projective Dynamic");
    panelsImGui.initImGui();
    // Camera panel
    cameraPanel.camera = &renderer.camera;
    panelsImGui.panels.push_back(&cameraPanel);
    // Simulation panel
    simulationPanel.engine = &engine;
    panelsImGui.panels.push_back(&simulationPanel);
    // Mesh selector panel
    meshSelectorPanel.onSelectMesh = [&](gumrender3d::Mesh*m) {
      meshDataViewPanel.mesh = m;
      meshDataTransformPanel.mesh = m;
      meshShadingPanel.mesh = m;
      dynamicBodyPanel.body = engine.getBodyForMesh(m);
    };
    meshSelectorPanel.meshes = &engine.getBodiesMeshes();
    panelsImGui.panels.push_back(&meshSelectorPanel);
    // Selected mesh panels
    panelsImGui.panels.push_back(&meshDataViewPanel);
    panelsImGui.panels.push_back(&meshDataTransformPanel);
    panelsImGui.panels.push_back(&meshShadingPanel);
    panelsImGui.panels.push_back(&dynamicBodyPanel);

    meshDataTransformPanel.onMeshDataTransform = [&](gumrender3d::Mesh*m) {
      Body * b = engine.getBodyForMesh(m);
      b->vertices = m->getVertices();
      b->init();
    };

    float mass = 5.0f;
    engine.method = DynamicMethod::PROJECTIVE_DYNAMICS;
    engine.niters = 3;
    engine.skipStepCount = 15;
    // engine.method = DynamicMethod::PBD;

    auto grid = engine.addGrid(4, {3.f,3.f,3.f}, {0,7,0}, {M_PI/2,0,0}, mass, false, "White plane", glm::vec4(COLOR_WHITE, 1.0f));
    std::shared_ptr<Constraint> ac0 = std::make_shared<AttachConstraint>(grid->vertices[0],0);
    grid->addConstraint(ac0);
    std::shared_ptr<Constraint> ac1 = std::make_shared<AttachConstraint>(grid->vertices[4],4);
    grid->addConstraint(ac1);
    grid->bendingConstraintStiffness = 0.9f;
    grid->distanceConstraintStiffness = 0.9f;

    // auto sp = engine.addSphere(10, {1.f,1.f,1.f}, {-3,5,0}, mass, false, "Green Sphere", glm::vec4(COLOR_GREEN, 1.0f));
    // sp->bendingConstraintStiffness = 0.8;
    // sp->distanceConstraintStiffness = 0.8;
    // sp->collisionConstraintStiffness = 0.9;
    // sp = engine.addSphere(10, {1.f,1.f,1.f}, {-3,8,0}, mass, false, "Yellow Sphere", glm::vec4(COLOR_YELLOW, 1.0f));
    // sp->bendingConstraintStiffness = 0.8;
    // sp->distanceConstraintStiffness = 0.8;
    // sp->collisionConstraintStiffness = 0.9;

    auto box = engine.addBox({0.5f,0.5f,0.5f}, {0,2.5,3}, mass, false, "Box Orange", glm::vec4(COLOR_ORANGE, 1.0f));
    box->bendingConstraintStiffness = 0.5f;
    box->distanceConstraintStiffness = 0.5f;

    // engine.addBox({0.5f,0.5f,0.5f}, {0,5.5,0}, mass, false, "Box Red", glm::vec4(COLOR_RED, 1.0f));
    // engine.addBox({0.5f,0.5f,0.5f}, {0.4,4,0}, mass, false, "Box Green", glm::vec4(COLOR_GREEN, 1.0f));
    // engine.addBox({0.5f,0.5f,0.5f}, {0,2.5,1.5}, mass, false, "Box Yellow", glm::vec4(COLOR_YELLOW, 1.0f));
    // engine.addBox({0.5f,0.5f,0.5f}, {0,1.5,-1.5}, mass, false, "Box Pink", glm::vec4(COLOR_PINK, 1.0f));

    engine.addBox({10.5f,0.1f,10.5f}, {0,0,0}, mass, true, "Plane", glm::vec4(COLOR_BLUE, 1.0f));
    // engine.addBox({5.0f,2.5f,0.5f}, {0,2.5f,3.0f}, mass, true, "Wall", glm::vec4(COLOR_BLUE, 1.0f));
  }

  void render(float deltaTime) override {
    // Update window dimensions
    controller.windowWidth = width;
    controller.windowHeight = height;
    renderer.camera.aspectRatio = float(width)/float(height);
    glViewport(0,0,width,height);

    // Clear frame buffer
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(pendingCapture) {
    // Start capture
      colorCapture.init(width, height);
      colorCapture.openCapture();
    }

    // Render scene
    engine.step(deltaTime);
    for(auto mesh : engine.getBodiesMeshes()) {
      renderer.renderMesh(mesh);
    }

    if(pendingCapture) {
      // End capture
      unsigned char * pixels = nullptr;
      int w = 0, h = 0;
      colorCapture.getColorData(&pixels, w, h);
      gumrender3d::Image image = gumrender3d::Image(w, h, 3, pixels);
      image.save(std::string("Capture")+std::to_string(captureID)+std::string(".png"));
      delete[] pixels;

      colorCapture.closeCapture();
      colorCapture.destroy();
      pendingCapture = false;
      captureID++;
    }

    // Render ImGui panel
    panelsImGui.renderImGui(deltaTime);
  };

  void beforeEnd() override {
    panelsImGui.clearImGui();
  }

  void onKeyDown(gumrender3d::KeyCode key, gumrender3d::KeyDevice device) override {
    if(key==gumrender3d::KeyCode::MOUSE_LEFT) {
      controller.onMousePress();
    }
    if(key==gumrender3d::KeyCode::MOUSE_RIGHT) {
      controller.onMousePressRight();
    }
  };

  void onKeyPush(gumrender3d::KeyCode key, gumrender3d::KeyDevice device) override {
    if(key==gumrender3d::KeyCode::C) {
      pendingCapture = true;
    }
  };
  void onMouseMove(float x, float y) override {
    controller.onMouseMove(x,y);
  };
  void onMouseScroll(float y) override {
    controller.onMouseScroll(y);
  };
};

int main() {
  AppWindow app;
  app.init();
  app.loop();
  app.end();
  return 0;
}

#include <GumRender3D.hpp>
#include "projective-dynamics/Body.hpp"

class DynamicEngine {
public:
  DynamicEngine() {}

  Body * addBox(
    const glm::vec3 & boxScale,
    const glm::vec3 & boxPos,
    float boxMass = 1.0f,
    bool isFixed = false,
    const std::string & name = "",
    const glm::vec4 color = glm::vec4(COLOR_ORANGE,1.f)
  );
  Body * addSphere(
    int d,
    const glm::vec3 & boxScale,
    const glm::vec3 & boxPos,
    float boxMass = 1.0f,
    bool isFixed = false,
    const std::string & name = "",
    const glm::vec4 color = glm::vec4(COLOR_ORANGE,1.f)
  );

  Body * addGrid(
    int N,
    const glm::vec3 & size,
    const glm::vec3 & pos,
    const glm::vec3 & planeRot,
    float boxMass = 1.0f,
    bool isFixed = false,
    const std::string & name = "",
    const glm::vec4 color = glm::vec4(COLOR_ORANGE,1.f)
  );

  DynamicMethod method = DynamicMethod::PBD;

  void step(float deltatime);

  inline std::vector<gumrender3d::Mesh*> & getBodiesMeshes() {
    return bodiesMesh;
  }

  inline Body * getBodyForMesh(gumrender3d::Mesh* m) {
    for(int i=0; i<bodiesMesh.size(); i++) {
      if(bodiesMesh[i] == m) {
        return bodies[i];
      }
    }
    return nullptr;
  }

  int skipStepCount = 10;
  int deltaFrac = 1;
  int niters = 10;

  bool sim = false;

private:
  std::vector<Body*> bodies;
  std::vector<gumrender3d::Mesh*> bodiesMesh;

  int skip = 0;
};

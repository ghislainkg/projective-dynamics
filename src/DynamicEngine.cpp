#include "DynamicEngine.hpp"

Body * DynamicEngine::addBox(
  const glm::vec3 & boxScale,
  const glm::vec3 & boxPos,
  float boxMass,
  bool isFixed,
  const std::string & name,
  const glm::vec4 color
) {
  std::vector<glm::vec3> vertices;
  std::vector<glm::uvec3> triangles;
  std::vector<float> masses;
  gumrender3d::createUnitCube(vertices, triangles);
  vertices = scale(vertices, boxScale);
  for(auto & v : vertices) v += boxPos;
  masses.clear();
  masses.reserve(vertices.size());
  for(auto & v : vertices) {
    masses.push_back(boxMass/vertices.size());
  }
  Body * bbox = new Body(vertices, masses, triangles);
  bbox->isFixed = isFixed;
  bbox->init();
  bbox->name = name;
  bodies.push_back(bbox);

  gumrender3d::Mesh * mbox = new gumrender3d::Mesh();
  mbox->updateVertices(vertices);
  mbox->updateTriangles(triangles);
  normalizeVectors(vertices);
  mbox->updateNormals(vertices);
  glBindVertexArray(0);
  mbox->color = color;
  mbox->edgeColor = glm::vec4(COLOR_BLACK, 1.0);
  mbox->edgeThickness = 0.003f; 
  mbox->name = name;
  bodiesMesh.push_back(mbox);
  return bbox;
}

Body * DynamicEngine::addSphere(
  int d,
  const glm::vec3 & sphereScale,
  const glm::vec3 & spherePos,
  float boxMass,
  bool isFixed,
  const std::string & name,
  const glm::vec4 color
) {
  std::vector<glm::vec3> vertices;
  std::vector<glm::uvec3> triangles;
  std::vector<float> masses;
  gumrender3d::createUnitSphere(d, vertices, triangles);
  vertices = scale(vertices, sphereScale);
  for(auto & v : vertices) v += spherePos;
  masses.clear();
  masses.reserve(vertices.size());
  for(auto & v : vertices) {
    masses.push_back(boxMass/vertices.size());
  }
  Body * bbox = new Body(vertices, masses, triangles);
  bbox->isFixed = isFixed;
  bbox->init();
  bbox->name = name;
  bodies.push_back(bbox);

  gumrender3d::Mesh * mbox = new gumrender3d::Mesh();
  mbox->updateVertices(vertices);
  mbox->updateTriangles(triangles);
  normalizeVectors(vertices);
  mbox->updateNormals(vertices);
  glBindVertexArray(0);
  mbox->color = color;
  mbox->edgeColor = glm::vec4(COLOR_BLACK, 1.0);
  mbox->edgeThickness = 0.003f; 
  mbox->name = name;
  bodiesMesh.push_back(mbox);
  return bbox;
}

float f(float x, float y) {
  return sin(x*2.0f*3.141526f) * sin(y*2.0f*3.141526f) * 0.1f;
}

void generate_grid(int N, std::vector<glm::vec3> &vertices, std::vector<glm::uvec3> &indices) {
  for (int j=0; j<=N; ++j)  {
    for (int i=0; i<=N; ++i) {
      float x = (float)i/(float)N;
      float y = (float)j/(float)N;
      float z = f(x, y);
      vertices.push_back(glm::vec3(x, y, z));
    }
  }
  for (int j=0; j<N; ++j) {
    for (int i=0; i<N; ++i) {
      int row1 = j * (N+1);
      int row2 = (j+1) * (N+1);  

      // triangle 1
      indices.push_back(glm::uvec3(row1+i, row1+i+1, row2+i+1)); 

      // triangle 2
      indices.push_back(glm::uvec3(row1+i, row2+i+1, row2+i)); 
    }
  }
}

Body * DynamicEngine::addGrid(
  int N,
  const glm::vec3 & planeScale,
  const glm::vec3 & planePos,
  const glm::vec3 & planeRot,
  float boxMass,
  bool isFixed,
  const std::string & name,
  const glm::vec4 color
) {
  std::vector<glm::vec3> vertices;
  std::vector<glm::uvec3> triangles;
  std::vector<float> masses;
  generate_grid(N, vertices, triangles);
  vertices = scale(vertices, planeScale);
  for(auto & p : vertices) {
    p = glm::eulerAngleXYZ(planeRot.x,planeRot.y,planeRot.z)*(glm::vec4(p,1.f));
  }
  for(auto & v : vertices) v += planePos;
  masses.clear();
  masses.reserve(vertices.size());
  for(auto & v : vertices) {
    masses.push_back(boxMass/vertices.size());
  }
  Body * bbox = new Body(vertices, masses, triangles);
  bbox->isFixed = isFixed;
  bbox->init();
  bbox->name = name;
  bodies.push_back(bbox);

  gumrender3d::Mesh * mbox = new gumrender3d::Mesh();
  mbox->updateVertices(vertices);
  mbox->updateTriangles(triangles);
  normalizeVectors(vertices);
  mbox->updateNormals(vertices);
  glBindVertexArray(0);
  mbox->color = color;
  mbox->edgeColor = glm::vec4(COLOR_BLACK, 1.0);
  mbox->edgeThickness = 0.003f; 
  mbox->name = name;
  bodiesMesh.push_back(mbox);
  return bbox;
}

void DynamicEngine::step(float deltatime) {
  if(sim) {
    deltatime = deltatime/skipStepCount;
    float dt = deltatime/deltaFrac;
    for(unsigned int k=0; k<deltaFrac; k++) {
      for(unsigned int i=0; i<bodies.size(); i++) {
        std::vector<Body*> others;
        for(unsigned int j=0; j<bodies.size(); j++) if(j!=i) others.push_back(bodies[j]);
        bodies[i]->nIters = niters;
        bodies[i]->method = method;
        bodies[i]->step(dt, others);

        bodiesMesh[i]->updateVertices(bodies[i]->getParticlesPos());
        // bodiesMesh[i]->recomputeNormals();
      }
    }
  }
}

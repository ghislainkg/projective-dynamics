#ifndef _RENDERING_
#define _RENDERING_

// GLM (for maths)
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/matrix_decompose.hpp>

// C++ STD library
#include <cstdlib>
#include <cstdio>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <string>
#include <cmath>
#include <memory>
#include <algorithm>
#include <functional>
#include <type_traits>
#include <random>

#define M_PI 3.14159265358979323846

// Glad (for OpenGL functions)
#include <glad/glad.h>

// GLWF (for OpenGL context and window)
#include <GLFW/glfw3.h>

// ImGui
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#define getOpenGLError(where) {int e = glGetError();if(e != GL_NO_ERROR) {std::cout << "GL Error detected : " << where << " : " << e << std::endl;assert(false);}}

inline std::ostream & operator<<(std::ostream & out, const glm::mat3 & m) {
  out << "|" << m[0][0] << "  " << m[1][0] << "  " << m[2][0] << "|" << std::endl;
  out << "|" << m[0][1] << "  " << m[1][1] << "  " << m[2][1] << "|" << std::endl;
  out << "|" << m[0][2] << "  " << m[1][2] << "  " << m[2][2] << "|" << std::endl;
  return out;
}

inline std::ostream & operator<<(std::ostream & out, const glm::mat4 & m) {
  out << "|" << m[0][0] << "  " << m[1][0] << "  " << m[2][0] << m[3][0] << "|" << std::endl;
  out << "|" << m[0][1] << "  " << m[1][1] << "  " << m[2][1] << m[3][1] << "|" << std::endl;
  out << "|" << m[0][2] << "  " << m[1][2] << "  " << m[2][2] << m[3][2] << "|" << std::endl;
  out << "|" << m[0][3] << "  " << m[1][3] << "  " << m[2][3] << m[3][3] << "|" << std::endl;
  return out;
}

inline std::ostream & operator<<(std::ostream & out, const glm::vec4 & v) {
  out << "(" << v.x << "  " << v.y << "  " << v.z << "  " << v.w << ")";
  return out;
}

inline std::ostream & operator<<(std::ostream & out, const glm::vec3 & v) {
  out << "(" << v.x << "  " << v.y << "  " << v.z << ")";
  return out;
}
inline std::ostream & operator<<(std::ostream & out, const glm::uvec3 & v) {
  out << "(" << v.x << "  " << v.y << "  " << v.z << ")";
  return out;
}

inline std::ostream & operator<<(std::ostream & out, const glm::vec2 & v) {
  out << "(" << v.x << "  " << v.y << ")";
  return out;
}
inline std::ostream & operator<<(std::ostream & out, const glm::uvec2 & v) {
  out << "(" << v.x << "  " << v.y << ")";
  return out;
}

inline std::string file2String(const std::string &filename) {
  std::ifstream t(filename.c_str());
  std::stringstream buffer;
  buffer << t.rdbuf();
  return buffer.str();
}

#define COLOR_RED glm::vec3(0.71f, 0.082f, 0.035f)
#define COLOR_GREEN glm::vec3(0.165f, 0.71f, 0.035f)
#define COLOR_BLUE glm::vec3(0.106f, 0.322f, 0.859f)
#define COLOR_YELLOW glm::vec3(0.859f, 0.835f, 0.106f)
#define COLOR_GRAY glm::vec3(0.388f, 0.388f, 0.388f)
#define COLOR_BLACK glm::vec3(0.0f, 0.0f, 0.0f)
#define COLOR_WHITE glm::vec3(1.0f, 1.0f, 1.0f)
#define COLOR_ORANGE glm::vec3(1.0f, 0.549f, 0.0f)
#define COLOR_PURPLE glm::vec3(0.463f, 0.0f, 1.0f)
#define COLOR_PINK glm::vec3(1.0f, 0.0f, 1.0f)

struct Ray {
  glm::vec3 start;
  glm::vec3 dir;
};

inline std::ostream & operator<<(std::ostream & out, const Ray & r) {
  out << "Ray: start=" << r.start << " direction=" << r.dir;
  return out;
}

inline glm::vec3 projectPointOnLine(
  const glm::vec3 & point,
  const glm::vec3 & lineA, const glm::vec3 & lineB
) {
  if(lineA==lineB) return lineA;
  glm::vec3 u = lineB - lineA;
  glm::vec3 v = point - lineA;
  float cos = glm::dot(glm::normalize(u), glm::normalize(v));
  return glm::length(v)*cos*u + lineA;
}

inline glm::mat3 getCrossProductMatrix(const glm::vec3 & v) {
  return glm::mat3(
    { 0, v.z, -v.y },
    { -v.z, 0, v.x },
    { v.y, -v.x, 0 }
  );
}

inline std::vector<glm::vec3> normalize(
  const std::vector<glm::vec3> & vs
) {
  auto res = vs;
  for(auto & v : res) {
    v = glm::normalize(v);
  }
  return res;
}

inline std::vector<glm::vec3> scale(
  const std::vector<glm::vec3> & vs,
  const glm::vec3 & s
) {
  auto res = vs;
  for(auto & v : res) {
    v = v*s;
  }
  return res;
}

inline float max(
  const std::vector<float> &values
) {
  float max = -FLT_MAX;
  for(auto v : values) {
    if(max<v) max = v;
  }
  return max;
}

inline float min(
  const std::vector<float> &values
) {
  float min = FLT_MAX;
  for(auto v : values) {
    if(min>v) min = v;
  }
  return min;
}

enum Axis {
  Xs, Ys, Zs
};

inline std::vector<glm::vec2> projectOnPlane(
  const std::vector<glm::vec3> & points,
  Axis axis
) {
  std::vector<glm::vec2> res;
  res.reserve(points.size());
  for(auto & p : points) {
    if(axis==Axis::Xs) res.push_back({p.y, p.z});
    if(axis==Axis::Ys) res.push_back({p.x, p.z});
    if(axis==Axis::Zs) res.push_back({p.x, p.y});
  }
  return res;
}

inline void normalizeVectors(std::vector<glm::vec3> & vectors) {
  float xMin = FLT_MAX, xMax = -FLT_MAX;
  float yMin = FLT_MAX, yMax = -FLT_MAX;
  float zMin = FLT_MAX, zMax = -FLT_MAX;
  for(auto & v : vectors) {
    xMin = std::min(xMin, v.x);
    xMax = std::max(xMax, v.x);
    yMin = std::min(yMin, v.y);
    yMax = std::max(yMax, v.y);
    zMin = std::min(zMin, v.z);
    zMax = std::max(zMax, v.z);
  }

  for(unsigned v=0; v<vectors.size(); v++) {
    vectors[v] = {
      (vectors[v].x - xMin)/(xMax-xMin),
      (vectors[v].y - yMin)/(yMax-yMin),
      (vectors[v].z - zMin)/(zMax-zMin)
    };
  }
}

struct MeshGeometry {
  std::vector<glm::vec3> vertices = {};
  std::vector<glm::vec3> normals = {};
  std::vector<glm::uvec3> triangles = {};
  std::vector<glm::uvec2> edges = {};

  std::vector<std::vector<unsigned int>> edgesTriangles = {};
  std::vector<std::vector<unsigned int>> trianglesEdges = {};

  void updateEdges() {
    edges.clear();
    unsigned int e1, e2;
    bool isNotIn;
    for(auto & t : triangles) {
      for(unsigned int i=0; i<3; i++) {
        e1 = t[i];
        e2 = t[(i+1)%3];
        isNotIn = true;
        isNotIn &= std::find(edges.begin(), edges.end(), glm::uvec2(e1,e2))==edges.end();
        isNotIn &= std::find(edges.begin(), edges.end(), glm::uvec2(e2,e1))==edges.end();
        if(isNotIn) {
          edges.push_back({e1, e2});
        }
      }
    }
    edgesTriangles.clear();
    for(auto & e : edges) {
      unsigned int tIndex = 0;
      edgesTriangles.push_back({});
      for(auto & t : triangles) {
        for(unsigned int i=0; i<3; i++) {
          unsigned int e0 = t[i];
          unsigned int e1 = t[(i+1)%3];
          if((e[0]==e0&&e[1]==e1) || (e[0]==e1&&e[1]==e0)) {
            edgesTriangles.back().push_back(tIndex);
          }
        }
        tIndex++;
      }
    }
    trianglesEdges.clear();
    for(auto & t : triangles) {
      unsigned int eIndex = 0;
      trianglesEdges.push_back({});
      for(auto & e : edges) {
        for(unsigned int i=0; i<3; i++) {
          unsigned int e0 = t[i];
          unsigned int e1 = t[(i+1)%3];
          if((e[0]==e0&&e[1]==e1) || (e[0]==e1&&e[1]==e0)) {
            trianglesEdges.back().push_back(eIndex);
          }
        }
        eIndex++;
      }
    }
  }

  void updateNormals() {
    normals.clear();
    normals.resize(vertices.size(), {0,0,0});
    for(auto & t : triangles) {
      glm::vec3 n = glm::cross(
        vertices[t[0]]-vertices[t[1]],
        vertices[t[0]]-vertices[t[2]]
      );
      normals[t[0]] += n;
      normals[t[1]] += n;
      normals[t[2]] += n;
    }
    for(auto & n : normals)
      n = glm::normalize(n);
  }

  void scale(const glm::vec3 & s) {
    for(auto & v : vertices) {
      v *= s;
    }
  }
  void translate(const glm::vec3 & t) {
    for(auto & v : vertices) {
      v += t;
    }
  }
  void rotate(const glm::vec3 & angles) {
    glm::mat4 m = glm::eulerAngleXYZ(angles.x, angles.y, angles.z);
    for(auto & v : vertices) {
      v = m*glm::vec4(v, 1.0f);
    }
  }

  void normalizeVertices() {
    normalizeVectors(vertices);
  }

  void centerVertices() {
    glm::vec3 c = {0,0,0};
    for(auto & v : vertices) c += v;
    c = c*(1.f/vertices.size());
    for(auto & v : vertices) v = v-c;
  }
};

inline int getRandom(int min, int max) {
  return min + (rand() % (max-min));
}

inline std::pair<float, float> getMinMaxAxis(
  const std::vector<std::vector<glm::vec3>*> & verticesList,
  Axis axis
) {
  float min = FLT_MAX;
  float max = -FLT_MAX;
  for(auto vertices : verticesList)
  for(auto & v : *vertices) {
    if(axis==Axis::Xs) {
      if(min > v.x) min = v.x;
      if(max < v.x) max = v.x;
    }
    else if(axis==Axis::Ys) {
      if(min > v.y) min = v.y;
      if(max < v.y) max = v.y;
    }
    else if(axis==Axis::Zs) {
      if(min > v.z) min = v.z;
      if(max < v.z) max = v.z;
    }
  }
  return {min, max};
}

inline float windingNumber(
  const std::vector<glm::vec2> & s,
  const glm::vec2 & p
) {
  float sumTheta = 0;
  glm::vec2 a,b;
  for(unsigned int i=0; i<s.size(); i++)
    sumTheta += glm::orientedAngle(glm::normalize(s[i]-p), glm::normalize(s[(i+1)%s.size()]-p));
  return (1.f/(2.f*M_PI)) * sumTheta;
}

inline void scale(std::vector<glm::vec2> & points, float c) {
  for(auto & p : points) p *= c;
}
inline void centerPoints(std::vector<glm::vec2> & points) {
  glm::vec2 c = {0,0};
  for(auto & p : points) c += p;
  c = c*(1.f/points.size());
  for(auto & p : points) p = p-c;
}
inline void normalizePoints(std::vector<glm::vec2> & points) {
  float xMin = FLT_MAX, xMax = -FLT_MAX;
  float yMin = FLT_MAX, yMax = -FLT_MAX;
  for(auto & v : points) {
    xMin = std::min(xMin, v.x);
    xMax = std::max(xMax, v.x);
    yMin = std::min(yMin, v.y);
    yMax = std::max(yMax, v.y);
  }

  for(unsigned v=0; v<points.size(); v++) {
    points[v] = {
      (points[v].x - xMin)/(xMax-xMin),
      (points[v].y - yMin)/(yMax-yMin)
    };
  }
}

#endif
